// Libraries
import React, { useState, useEffect } from 'react';
import { 
  BrowserRouter as Router, Switch, Route 
} from "react-router-dom";
import jwt_decode from "jwt-decode";
import { URL } from "./config";

// Components
import Navbar from "./components/layouts/Navbar";
import Home from "./components/Home";
import Landing from "./components/Landing";
import Footer from "./components/layouts/Footer";
import Login from "./components/forms/SignInSide";
import Register from "./components/forms/Register";
import PostReg from "./components/PostRegistration";
import Confirmation from "./components/Confirmation";
import AddEvent from "./components/forms/AddEvent";
import ViewEvent from "./components/ViewEvent";
import BookEvent from "./components/forms/BookEvent";
import Stripe from "./components/forms/Stripe";
import ViewBookings from "./components/ViewBookings";

function App() {
  const [events, setEvents] = useState([])
  const [user, setUser] = useState({});
  const [token, setToken] = useState("");

  // useEffect( () => {
  //   setUser(JSON.parse(localStorage.getItem("user")))
  //   setToken(localStorage.getItem("user"))
  // })

  useEffect( () => {
    fetch(`${URL}/events`)
    .then(res => res.json())
    .then(data => {
      setEvents(data)
    })

    setUser(JSON.parse(localStorage.getItem("user")))
    setToken(localStorage.getItem("user"))

  }, []);

  const logoutHandler = () => {
    localStorage.clear()
    setUser({})
    setToken("")
    window.location.href="/home"
  };

  return (
    <Router>
      
      <Navbar 
        user={user}
        token={token}
        logoutHandler={logoutHandler}
      />

      <Switch>

        <Route exact path="/">
          <Landing />
        </Route>        

        <Route exact path="/home">
          <Home events={events} />
        </Route>

        <Route exact path="/login">
          <Login />
        </Route>

        <Route exact path="/register">
          <Register />
        </Route>

        <Route exact path="/post-registration">
          <PostReg />
        </Route>

        <Route exact path="/users/:id/:token">
          <Confirmation />
        </Route>

        <Route exact path="/add-event">
          <AddEvent />
        </Route>

        <Route exact path="/events/:id">
          <ViewEvent />
        </Route>

        <Route exact path="/events/:id/booking">
          <BookEvent />
        </Route>

        <Route exact path="/stripe">
          <Stripe />
        </Route>

        <Route exact path="/bookings/:id">
          <ViewBookings />
        </Route>

      </Switch>


    </Router>
  );
}

export default App;
