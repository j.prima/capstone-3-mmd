import React, { useState, useEffect, Fragment } from "react";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { Carousel } from "react-bootstrap";
import EditEvent from "./forms/EditEvent";
import { URL } from "../config";
import Footer from "./layouts/Footer";

const ViewEvent = () => {
	let {id} = useParams();
	const [event, setEvent] = useState({});
	const [editing, setEditing] = useState(false);
	const [categories, setCategories] = useState([]);
	const [users, setUsers] = useState([]);
	const user = JSON.parse(localStorage.getItem("user"))

	useEffect( () => {
		fetch(`${URL}/events/${id}`)
		.then(res => res.json())
		.then(data => {
			setEvent(data)
		})
	}, [event]);

	useEffect( () => {
		fetch(`${URL}/categories`)
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})
	}, []);

	useEffect( () => {
		fetch(`${URL}/users`)
		.then(res => res.json())
		.then(data => {
			setUsers(data)
		})
	}, []);

	const deleteEvent = () => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				fetch(`${URL}/events/${id}`, {
					method: "DELETE"
					// headers: {
					// 	"x-auth-token": localStorage.getItem("token")
					// }
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						"Deleted",
						"Event has been deleted.",
						"success"
					)

					window.location.href = "/home"
				})
			}
		})
	}

	const getDate = (date) => {
		let dt = new Date(date)
		return dt.getFullYear() +"/"+ dt.getMonth() +"/"+ dt.getDate()
	}

	return (
		<Fragment>

		<div className="container mx-auto my-5 col-md-12">
			{editing ? <EditEvent setEditing={setEditing} event={event} /> :
				<div className="row mx-auto">

					<div className="col-md-6">
						<h2 className="font-weight-bold">{event.name}</h2>
						{
							categories ? 
						    	(categories.map(category => (
						    		category._id === event.categoryId ? 
						    		<p key={category._id}>{category.name}</p> : null
					    		))) :
					    	null
						}
						{
							users ? 
						    	(users.map(user => (
						    		user._id === event.userId ? 
						    		<p key={user._id}>Organized by: <strong className="text-uppercase">{user.fullname}</strong></p> : null
					    		))) :
					    	null
						}
						<hr />
						
						<h6 className="font-weight-bold">Description</h6>
						<p>{event.description}</p>

						<h6 className="font-weight-bold">Date of Event </h6>
						<p>{getDate(event.dateOfEvent)}</p>

						<h6 className="font-weight-bold">Capacity </h6>
						<p>{event.capacity}</p>

						<h6 className="font-weight-bold">Rate</h6> 
						<p>PHP {event.rate}/pax</p>

						{
							user ? 
								user.authId !== "5e9bf8e3ffd86ed245693d57" && user._id === event.userId ? 
									<div className="card-footer text-center">
										<button type="button" className="btn btn-warning w-25 mr-3" onClick={() => setEditing(!editing)}>Edit</button>
										<button type="button" className="btn btn-danger w-25" onClick={deleteEvent}>Delete</button>
									</div>
								: null
							: 
								<div className="card-footer text-center">
									<Link className="btn text-white w-25" to="/login" style={{backgroundColor: "#3F51B5"}}>Sign in to book</Link>
								</div>
						}
					</div>
					
					<div className="col-md-6">
						<Carousel>
							{event.images ? event.images.map(img => (
							 	<Carousel.Item>
							 		<img
							 			src={`${URL}/images/${img}`}
										height="400px"
										width="100%"
										alt="..."
										style={{objectFit: "contain"}}
							 		/>
							 	</Carousel.Item>
							)) : null}
						</Carousel>
					</div>

				</div>
			}
		</div>
		
		<Footer />

		</Fragment>
	)
};

export default ViewEvent;