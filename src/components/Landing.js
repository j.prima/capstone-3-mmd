import React,{ Fragment } from "react";
import { Link } from "react-router-dom";

const Landing = () => {
	const style = {
		position: "absolute",
		top: "0",
		left: "0",
		opacity: ".7"
	}

	const styleLabel = {
		position: "absolute",
		top: "90vh"
	}

	const styleSlogan = {
		fontFamily: "Indie Flower, cursive",
		letterSpacing: "2px",
		backgroundColor: "rgba(,,,#EEEEEE)",
		padding: "20px",
		borderRadius: "15px",
		textShadow: "2px 2px rgba(176, 176, 176, .7)",
		fontSize: "50px"
	}

	const styleLink = {
		position: "absolute",
		top: "85vh",
		left: "75vw",
		fontFamily: "Indie Flower, cursive",
		letterSpacing: "2px",
		backgroundColor: "rgba(,,,#EEEEEE)",
		padding: "20px",
		textShadow: "2px 2px rgba(176, 176, 176, .7)",
		fontSize: "30px",
		opacity: ".7"
	}

	return (
		<Fragment>
			<div className="container-fluid" style={{position: "relative"}}>
				<video className="col-12 p-0" id="myvideo" autoplay="true" muted="true" loop="true" width="100%" style={style}>
				  <source src="/videos/vid2.mp4" type="video/mp4" />
				</video>
				<a className="btn btn-primary text-center" style={styleLink} href="/home">Make My Day! :)</a>
			</div>
		</Fragment>
	)
}

export default Landing;