import React from "react";
import StripeCheckout from "react-stripe-checkout";
import { PUBLISHABLE_KEY, URL } from "../../config";

const Stripe = ({amount, booking}) => {
    const user = JSON.parse(localStorage.getItem('user'))

    const checkout = (token) => {
        let body = {
            token,
            amount,
            booking
        }

        fetch(`${URL}/bookings/stripe`, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.status === 200) {
                window.location.href=`/bookings/${user._id}`
            } else {
                console.log(data.err)
            }
        })
    }

    return(
        <StripeCheckout className="btn btn-danger"
            stripeKey={PUBLISHABLE_KEY}
            label="Card Payment"
            name="Make My Day"
            description="Live an eventful life!"
            panelLabel="Pay"
            amount={amount*100}
            billingAddress={false}
            currency="PHP"
            allowRememberMe={false}
            token={checkout}
        />
    )
}

export default Stripe
