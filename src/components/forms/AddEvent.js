import React, { useState, useEffect, Fragment } from "react";
import Swal from "sweetalert2";
import { URL } from "../../config";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import IconButton from '@material-ui/core/IconButton';
import Footer from "../layouts/Footer";
import PhotoCamera from '@material-ui/icons/PhotoCamera';

const AddEvent = () => {
	const [formData, setFormData] = useState({});
	const [categories, setCategories] = useState([]);
	const [date, setDate] = useState(null);

	const useStyles = makeStyles((theme) => ({
  	  root: {
  	    height: '100vh',
  	  },
  	  image: {
  	    backgroundImage: 'url(https://images.pexels.com/photos/705164/computer-laptop-work-place-camera-705164.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260)',
  	    backgroundRepeat: 'no-repeat',
  	    backgroundColor:
  	      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
  	    backgroundSize: 'cover',
  	    backgroundPosition: 'center',
  	  },
  	  paper: {
  	    margin: theme.spacing(8, 4),
  	    display: 'flex',
  	    flexDirection: 'column',
  	    alignItems: 'center',
  	  },
  	  avatar: {
  	    margin: theme.spacing(1),
  	    backgroundColor: "#3F51B5",
  	  },
  	  form: {
  	    width: '100%', // Fix IE 11 issue.
  	    marginTop: theme.spacing(1),
  	  },
  	  submit: {
  	    margin: theme.spacing(3, 0, 2),
  	  },
  	  formControl: {
  	  	marginTop: theme.spacing(1),
	    width: "100%"
	  },
	  selectEmpty: {
	    marginTop: theme.spacing(2),
	  },
  	}));

  	const classes = useStyles();

	useEffect( () => {
		fetch(`${URL}/categories`)
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})
	}, [])

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		setFormData({
			...formData,
			images: e.target.files
		})
	}

	const handleDate = (dt) => {
		setDate(dt)
		setFormData({
			...formData,
			dateOfEvent: dt
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		
		let user = JSON.parse(localStorage.getItem('user'));
		let event = new FormData();
		event.append('name', formData.name)
		event.append('description', formData.description)
		event.append('userId', user._id)
		event.append('categoryId', formData.categoryId)
		event.append('capacity', formData.capacity)
		event.append('dateOfEvent', formData.dateOfEvent)
		event.append('location', formData.location)
		event.append('rate', formData.rate)

		if (typeof formData.images === 'object') {
			for(let i = 0; i < formData.images.length; i++) {
				event.append('images', formData.images[i])
			}
		} else {
			Swal.fire({
				icon: "error",
				text: "Please upload atleast 1 image"
			})
			return
		}

		fetch(`${URL}/events`, {
			method: "POST",
			body: event,
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: "success",
					text: data.message
				})

				window.location.href = "/home"
			} else {
				Swal.fire({
					icon: "error",
					text: "Please check your inputs"
				})
				console.log(event)
			}
		})
	}

	return (
		<Fragment>
		<div className="container">
			<div className="row mt-5">
				<div className="col-6 offset-3 p-3">
					<form className={classes.form} noValidate encType="multipart/form-data">
				        <Typography className="text-center mb-4" component="h1" variant="h4">
				            Event Registration
				        </Typography>

						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							autoFocus
							id="name"
							label="Event Name"
							name="name"
							onChange={onChangeHandler}
			            />

						<TextField
							variant="outlined"
							margin="normal"
							fullWidth
							multiline
							rows={4}
							id="description"
							label="Description"
							name="description"
							onChange={onChangeHandler}
			            />

			            <FormControl variant="outlined" className={classes.formControl} required>
							<InputLabel id="categoryId">Event Type</InputLabel>
							<Select
								margin="normal"
				                required
				                fullWidth
								labelId="categoryId"
								id="categoryId"
								name="categoryId"
								label="Event Type"
								onChange={onChangeHandler}
							>
								{categories.map(category => (
									<MenuItem value={category._id} key={category._id}>
										{category.name}
									</MenuItem>
								))}
							</Select>
						</FormControl>

						<div className="form-group mt-3">
							<DatePicker
								className="form-control"
						      	selected={date}
						      	onChange={dt => handleDate(dt)}
						      	placeholderText="Date of Event *"
						      	style={{width: "100%"}}
						    />
						</div>

			            <TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="location"
							label="Location"
							id="location"
							className="mt-0"
							onChange={onChangeHandler}
			            />

			            <TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="capacity"
							label="Venue Capacity"
							id="capacity"
							onChange={onChangeHandler}
			            />

			            <TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="rate"
							label="Rate"
							id="rate"
							type="number"
							onChange={onChangeHandler}
			            />

			            <Button
			            	className={classes.submit}
			            	type="button"
							variant="outlined"
							component="label"
							color="primary"
							fullWidth
			            >
							Upload File
							<input
								type="file"
								name="images"
								multiple
								style={{ display: "none" }}
								onChange={handleFile}
							/>
							<label htmlFor="icon-button-file">
								<IconButton color="primary" aria-label="upload picture" component="span" className={classes.selectEmpty}>
									<PhotoCamera />
								</IconButton>
							</label>
			            </Button>

			            <Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							className={classes.selectEmpty}
							onClick={onSubmitHandler}
			            >
			              Add event
			            </Button>
					</form>
				</div>
			</div>
		</div>
		<Footer />
		</Fragment>
	)
};

export default AddEvent;