import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { Carousel } from "react-bootstrap";
import { URL } from "../../config";
import Stripe from "./Stripe";
import Button from '@material-ui/core/Button';

const BookEvent = () => {
	let {id} = useParams();
	const [event, setEvent] = useState({});
	const [categories, setCategories] = useState([]);
	const [users, setUsers] = useState([]);
	const [total, setTotal] = useState(0)
	const user = JSON.parse(localStorage.getItem("user"))
	// const [attendees, setAttendees] = useState(0);
	const [booking, setBooking] = useState({});

	useEffect( () => {
		fetch(`${URL}/events/${id}`)
		.then(res => res.json())
		.then(data => {
			setEvent(data)
		})
	}, [event]);

	useEffect( () => {
		fetch(`${URL}/categories`)
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})
	}, []);

	useEffect( () => {
		fetch(`${URL}/users`)
		.then(res => res.json())
		.then(data => {
			setUsers(data)
		})
	}, []);

	const getDate = (date) => {
		let dt = new Date(date)
		return dt.getFullYear() +"/"+ dt.getMonth() +"/"+ dt.getDate()
	}

	const onSelectChange = (e) => {
		// setAttendees(e.target.value)

		let tot = Number(parseFloat(event.rate * e.target.value).toFixed(2))
		setTotal(tot)

		let book = {
			'userId': user._id,
			'eventId': event._id,
			'statusId': "5ea8359aedefdc4f0fe9e0e6",
			'attendees': e.target.value,
			'paymentMode': "stripe",
			'total': tot
		}

		setBooking(book)
	}

	const cmbAttendees = (
		<select className="form-control w-50 mb-2" onChange={onSelectChange}>
        		{(() => {
					const options = [];
					options.push(<option disabled selected defaultValue value={0}>Select pax</option>)
					for (let i = 1; i <= 20; i++) {
						options.push(<option value={i}>{i}</option>);
					}
		          return options;
        		})()}
  		</select>
	)

	const onClickHandler = (e) => {
		e.preventDefault();
		console.log(total)
		if (total === 0) {
			Swal.fire({
				icon: "error",
				text: "Please input quantity"
			})
			return
		}

		if (e.target.value === "cancel") {
			window.location.href = "/home"
		} else if (e.target.value === "submit") {
			fetch(`${URL}/bookings`, {
				method: "POST",
				body: JSON.stringify(booking),
				headers: {
					"Content-Type": "application/json",
					"x-auth-token": localStorage.getItem("token")	
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data.status === 200) {
					Swal.fire({
					  title: "Booking successful",
					  text: "Proceed to check out?",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, proceed'
					}).then((result) => {
						if (result.value) {
							window.location.href = `/bookings/${user._id}`
						} else {
							window.location.href = "/bookings"
						}
					})
				} else {
					Swal.fire({
						icon: "error",
						text: "Please check your inputs"
					})
					console.log(event)
				}
			})
		}
	}

	return (
		<div className="container mx-auto my-5 col-md-12">
			<div className="row mx-auto">

				<div className="col-md-6">
					<h2 className="font-weight-bold">{event.name}</h2>
					{
						categories ? 
					    	(categories.map(category => (
					    		category._id === event.categoryId ? 
					    		<p key={category._id}>{category.name}</p> : null
				    		))) :
				    	null
					}
					<p>
					{
						users ? 
					    	(users.map(user => (
					    		user._id === event.userId ? 
					    		<p key={user._id}>Organized by: <strong>{user.fullname}</strong></p> : null
				    		))) :
				    	null
					}
					</p>
					<hr />
					
					<h6 className="font-weight-bold">Description </h6>
					<p>{event.description}</p>

					<h6 className="font-weight-bold">Date of Event </h6>
					<p>{getDate(event.dateOfEvent)}</p>

					<table className="table table-hover">
						<thead>
							<tr>
								<th>Attendees</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{cmbAttendees}</td>
								<td>{total}</td>
							</tr>
						</tbody>
					</table>

					<div className="card-footer text-center">
						<button 
							type="button" 
							className="btn w-25 mr-3 text-white" 
							style={{backgroundColor: "#3F51B5"}}
							onClick={onClickHandler}
							value="submit"
						>OTC</button>
						<button 
							type="button" 
							className="btn w-25 mr-3 btn-danger text-white"
							onClick={onClickHandler}
							value="cancel"
						>Find another</button>
						<button
			            	type="button"
			            	className="btn w-25 mr-3 btn-primary"
							color="primary"
							value="card"
			            >
							<Stripe 
								className="d-none"
								amount={total} 
								booking={booking} 
								style={{ display: "none" }} 
							/>
						</button>
					</div>
				</div>
				
				<div className="col-md-6">
					<Carousel>
						{event.images ? event.images.map(img => (
						 	<Carousel.Item>
						 		<img
						 			src={`${URL}/images/${img}`}
									height="400px"
									width="100%"
									alt="..."
									style={{objectFit: "contain"}}
						 		/>
						 	</Carousel.Item>
						)) : null}
					</Carousel>
				</div>

			</div>
		</div>
	)
};

export default BookEvent;