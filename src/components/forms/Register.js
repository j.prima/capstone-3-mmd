import React, { useState, Fragment } from "react";
import Swal from "sweetalert2";
import { URL } from "../../config";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Footer from "../layouts/Footer";

const Register = () => {
	const [formData, setFormData] = useState({});
	const [userType, setUserType] = useState("user");

  	const useStyles = makeStyles((theme) => ({
  	  root: {
  	    height: '100vh',
  	  },
  	  image: {
  	    backgroundImage: 'url(https://images.pexels.com/photos/705164/computer-laptop-work-place-camera-705164.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260)',
  	    backgroundRepeat: 'no-repeat',
  	    backgroundColor:
  	      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
  	    backgroundSize: 'cover',
  	    backgroundPosition: 'center',
  	  },
  	  paper: {
  	    margin: theme.spacing(8, 4),
  	    display: 'flex',
  	    flexDirection: 'column',
  	    alignItems: 'center',
  	  },
  	  avatar: {
  	    margin: theme.spacing(1),
  	    backgroundColor: "#3F51B5",
  	  },
  	  form: {
  	    width: '100%', // Fix IE 11 issue.
  	    marginTop: theme.spacing(1),
  	  },
  	  submit: {
  	    margin: theme.spacing(3, 0, 2),
  	  },formControl: {
	    margin: theme.spacing(1),
	    minWidth: "420",
	    width: "95%",
	  },
	  selectEmpty: {
	    marginTop: theme.spacing(2),
	  },
  	}));

	const bg = {
	  	backgroundImage: "url(https://images.pexels.com/photos/705164/computer-laptop-work-place-camera-705164.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260)",
	  	backgroundRepeat: "no-repeat",
    	backgroundColor: "white",
    	backgroundSize: "cover",
    	backgroundPosition: "center",
    	height: "100vh"
	};

	const formBg = {
		backgroundColor: "rgba(255, 255, 255, 1)"
	};

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	};

	const onSubmitHandler = (e) => {
		console.log(formData)
		e.preventDefault()
		fetch(`${URL}/users`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: "success",
					text: data.message,
					timer: 3500
				})

				window.location.href="/post-registration"
			} else {
				Swal.fire({
					icon: "error",
					text: data.message,
					timer: 3500,
					showConfirmButton: false
				})
			}
		})
	};

	const onSelectChangeHandler = (e) => {
		setUserType(e.target.value)
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

  	const classes = useStyles();

	return (
		<Fragment>
		<div className="container-fluid" style={bg}>
			<div className="row mx-auto my-auto mt-5">
				<div className="col-6 offset-3 mt-5 p-3" style={formBg}>
					<form className={classes.form} noValidate>
				        <Typography className="text-center" component="h1" variant="h5">
				            Registration
				        </Typography>

						<FormControl className={classes.formControl}>
							<InputLabel id="authId">Account Type</InputLabel>
							<Select
								margin="normal"
				                required
				                fullWidth
				                autoFocus
								labelId="authId"
								id="authId"
								name="authId"
								onChange={onSelectChangeHandler}
							>
								<MenuItem value="organizer">Organizer</MenuItem>
								<MenuItem value="user">Customer</MenuItem>
							</Select>
						</FormControl>

						<TextField
			              variant="outlined"
			              margin="normal"
			              required
			              fullWidth
			              id="fullname"
			              label={userType === "user" ? "Full Name" : "Organization Name"}
			              name="fullname"
			              onChange={onChangeHandler}
			            />

						<TextField
			              variant="outlined"
			              margin="normal"
			              required
			              fullWidth
			              id="email"
			              label="Email"
			              name="email"
			              onChange={onChangeHandler}
			            />

			            <TextField
			              variant="outlined"
			              margin="normal"
			              required
			              fullWidth
			              name="password"
			              label="Password"
			              type="password"
			              id="password"
			              onChange={onChangeHandler}
			            />

			            <TextField
			              variant="outlined"
			              margin="normal"
			              required
			              fullWidth
			              name="password2"
			              label="Confirm Password"
			              type="password"
			              id="password2"
			              onChange={onChangeHandler}
			            />

			            <Button
			              type="submit"
			              fullWidth
			              variant="contained"
			              color="primary"
			              className={classes.submit}
			              onClick={onSubmitHandler}
			            >
			              Register
			            </Button>
					</form>
				</div>
			</div>
		</div>

		<Footer />
		</Fragment>

	)
}

export default Register;