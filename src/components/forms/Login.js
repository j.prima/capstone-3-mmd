import React, { useState, Fragment } from "react";
import Swal from "sweetalert2";
// import { getReqAtts } from "./Constants";
import { URL } from "../../config";
import Footer from "../layouts/Footer";

const Login = () => {
	const [formData, setFormData] = useState({});

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const onSubmitHandler = (e) => {
		fetch(`${URL}/users/login`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.auth) {
				Swal.fire({
					icon: "success",
					title: data.message,
					showConfirmButton: false,
					timer: 2000
				})

				localStorage.setItem("user", JSON.stringify(data.user))
				localStorage.setItem("token", data.token)

				window.location.href="/home"
			} else {
				Swal.fire({
					icon: "error",
					title: data.message,
					showConfirmButton: false,
					timer: 2000
				})
			}
		})
		.catch(error => console.log(error))
	}

	return (
		<Fragment>

		<div className="container">
			<form className="mx-auto mt-5 col-sm6" onSubmit={onSubmitHandler}>
				<div className="form-group">
					<label htmlFor="email">Email</label>
					<input 
						className="form-control"
						type="email" 
						id="email" 
						name="email" 
						onChange={onChangeHandler} 
					/>
				</div>
				<div className="form-group">
					<label htmlFor="password">Password</label>
					<input 
						className="form-control"
						type="password" 
						id="password" 
						name="password" 
						onChange={onChangeHandler} 
					/>
				</div>
				<button className="btn btn-success">
					Login
				</button>
			</form>
		</div>

		<Footer />

		</Fragment>
	)
}

export default Login;