import React from "react";
import { Link } from "react-router-dom";
import { URL } from "../../config";

const Footer = () => {
  return (
        <footer className="site-footer p-3" style={{position: "relative", backgroundColor: "#3F51B5"}}>
        
          <div className="container" style={{color: "white"}}>
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <h6>About</h6>
                <p className="text-justify">
                  Make My Day is a project created by Jep Prima, an inspiring web developer. This project was created in thought of making a website so easy to use and navigate through to book events. The application is still in progress, and is subject to changes often. Support the creator by visiting and giving feedbacks on the website. Thank you! :)
                </p>
              </div>

              <div className="col-xs-6 col-md-3">
                <h6>Categories</h6>
                <ul className="footer-links">
                  <li><Link to="/categories/seminars-conventions">Seminar/Conference</Link></li>
                  <li><Link to="/exhibits">Exhibit</Link></li>
                  <li><Link to="/workshops">Workshop</Link></li>
                  <li><Link to="/festivals">Festival</Link></li>
                  <li><Link to="/sports">Sports</Link></li>
                </ul>
              </div>

              <div className="col-xs-6 col-md-3">
                <h6>Quick Links</h6>
                <ul className="footer-links">
                  <li><Link to="/">About Us</Link></li>
                  <li><Link to="/">Contact Us</Link></li>
                  <li><Link to="/">Contribute</Link></li>
                  <li><Link to="/">Privacy Policy</Link></li>
                  <li><Link to="/">Sitemap</Link></li>
                </ul>
              </div>
            </div>
            <hr />
          </div>

          <div className="container" style={{color: "white"}}>
            <div className="row">
              <div className="col-md-8 col-sm-6 col-xs-12">
                <p className="copyright-text">Copyright &copy; 2020 All Rights Reserved by Made My Day.</p>
              </div>
              <div className="col-md-4 col-sm-6 col-xs-12">
                <ul className="social-icons">
                  <li><a className="facebook" href="https://www.facebook.com"><i className="fa fa-facebook"></i></a></li>
                  <li><a className="twitter" href="https://www.twitter.com"><i className="fa fa-twitter"></i></a></li>
                  <li><a className="dribbble" href="https://dribbble.com/"><i className="fa fa-dribbble"></i></a></li>
                  <li><a className="linkedin" href="https://linkedin.com"><i className="fa fa-linkedin"></i></a></li>   
                </ul>
              </div>
            </div>
          </div>

    </footer>
  )
};

export default Footer;