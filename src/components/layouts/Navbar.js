import React, { useState, Fragment } from "react";
import {
	Collapse, Navbar, NavbarToggler, NavbarBrand,
	Nav
} from "reactstrap";
import { Link } from "react-router-dom";

const TopNav = ({user, token, logoutHandler}) => {
	const [collapsed, setCollapsed] = useState(true);
	let guestLinks;
	let orgLinks;
	let cusLinks;
	let adminLinks;
	const isLoggedIn = user && token
	const isAdmin = isLoggedIn && user.authId === "5e9bf8c6ffd86ed245693d4a"
	const isOrgnzr = isLoggedIn && user.authId === "5e9bf8d4ffd86ed245693d53"

	if (!isLoggedIn) {
		guestLinks = (
			<Fragment>
				<li className="nav-item">
					<Link to="/login" className="nav-link active">
						Login
					</Link>
				</li>
				<li className="nav-item">
					<Link to="/register" className="nav-link active">
						Register
					</Link>
				</li>
			</Fragment>
		)
	}

	if (isAdmin) {
		adminLinks = (
			<Fragment>
				<li className="nav-item">
	                <Link to={`/bookings/${user._id}`} className="nav-link active">Booking</Link>
				</li>
				<li className="nav-item">
					<Link to="/logout" className="nav-link active" onClick={logoutHandler}>
	 					Logout
	 				</Link>
				</li>
			</Fragment>
		)
	} 

	if (isOrgnzr) {
		orgLinks = (
			<Fragment>
				<li className="nav-item">
					<Link to="/add-event" className="nav-link active">Add Event</Link>
				</li>
				<li className="nav-item">
	                <Link to={`/bookings/${user._id}`} className="nav-link active">Booking</Link>
				</li>
				<li className="nav-item">
					<Link to="/logout" className="nav-link active" onClick={logoutHandler}>
	 					Logout
	 				</Link>
				</li>
			</Fragment>
		)
	}

	if (isLoggedIn && !isAdmin && !isOrgnzr) {
		cusLinks = (
			<Fragment>
				<li className="nav-item">
	                <Link to={`/bookings/${user._id}`} className="nav-link active">Booking</Link>
				</li>
				<li className="nav-item">
					<Link to="/logout" className="nav-link active" onClick={logoutHandler}>
	 					Logout
	 				</Link>
				</li>
			</Fragment>
		)
	}

	return (
		<Navbar dark expand="md" style={{fontFamily: "Indie Flower, cursive", fontSize: "24px", backgroundColor: "#3F51B5"}}>
			<Link to="/">
				<NavbarBrand style={{fontSize: "26px"}}>
					<img src={"/images/logo_size 165x165.jpg"} height="50px" width="50px" alt="" /> Make My Day
				</NavbarBrand>
			</Link>
			<NavbarToggler onClick={() => setCollapsed(!collapsed)} className="mr-2" />
			<Collapse isOpen={!collapsed} navbar className="justify-content-end">
				<Nav navbar className="justify-content-end">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
			                <Link to="/home" className="nav-link active">Home</Link>
						</li>
						{adminLinks}
						{guestLinks}
						{orgLinks}
						{cusLinks}
					</ul>
				</Nav>
			</Collapse>
		</Navbar>
	)
}

export default TopNav;