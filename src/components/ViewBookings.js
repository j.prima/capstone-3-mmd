import React, { useState, useEffect, Fragment } from "react";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { URL } from "../config";
import Footer from "./layouts/Footer";

const ViewBookings = () => {
	const {id} = useParams()
	const [bookings, setBookings] = useState([]);
	const [events, setEvents] = useState([]);
	const [users, setUsers] = useState([]);
	const [statuses, setStatuses] = useState([]);
	const user = JSON.parse(localStorage.getItem("user"))

	useEffect( () => {
		let url = `${URL}/bookings/${id}`
		if (user.authId === "5e9bf8c6ffd86ed245693d4a") {
			url = `${URL}/bookings`
		}

		fetch(url)
		.then(res => res.json())
		.then(data => {
			setBookings(data)
		})
	}, [bookings])

	useEffect( () => {
		fetch(`${URL}/events`)
		.then(res => res.json())
		.then(data => {
			setEvents(data)
		})
	}, [events]);

	useEffect( () => {
		fetch(`${URL}/users`)
		.then(res => res.json())
		.then(data => {
			setUsers(data)
		})
	}, [users]);

	useEffect( () => {
		fetch(`${URL}/statuses`)
		.then(res => res.json())
		.then(data => {
			setStatuses(data)
		})
	}, [statuses]);

	const getDate = (date) => {
		let dt = new Date(date)
		return dt.getFullYear() +"/"+ dt.getMonth() +"/"+ dt.getDate()
	}

	const onCancelHandler = (id) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, cancel it!'
		}).then((result) => {
			if (result.value) {
				fetch(`${URL}/bookings/${id}`, {
					method: "PUT"
					// headers: {
					// 	"x-auth-token": localStorage.getItem("token")
					// }
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						"Cancelled",
						"Booking has been cancelled.",
						"success"
					)
				})
			}
		})
	}

	const onPayHandler = (id) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, proceed with payment!'
		}).then((result) => {
			if (result.value) {
				fetch(`${URL}/bookings/${id}/payment`, {
					method: "PUT"
					// headers: {
					// 	"x-auth-token": localStorage.getItem("token")
					// }
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						"Paid",
						"Booking has been paid.",
						"success"
					)
				})
			}
		})
	}

	const showBookings = bookings.map(booking => (
		<Fragment>
			<tr>
				<td>{booking._id}</td>
				{
					events ? 
				    	(events.map(event => (
				    		event._id === booking.eventId ? 
				    			<td className="text-nowrap" key={event._id}>{event.name}</td> : null
		    			))) 
	    			: null
				}
				{
					events ? 
				    	(events.map(event => (
				    		event._id === booking.eventId ? 
				    		(users.map(user => (
				    			user._id === event.userId ?
				    				<td className="text-nowrap" key={user._id}>{user.fullname}</td> : null
			    			)))
				    		: null
		    			))) 
	    			: null
				}
				<td>{getDate(booking.dateBooked)}</td>
				<td>{booking.attendees}</td>
				<td>{booking.total}</td>
				{
					statuses ?
						(statuses.map(status => (
							status._id === booking.statusId ?
								<td className="text-nowrap" key={status._id}>{status.name}</td> : null
						)))
					: null
				}
				<td>
				{
					booking.statusId !== "5ea835b7edefdc4f0fe9e95c" ? 
					<button 
						type="submit" 
						className="btn btn-danger" 
						onClick={() => {onCancelHandler(booking._id)}}>Cancel
					</button> : null
				}
				</td>
				<td>
				{
					booking.statusId === "5ea8359aedefdc4f0fe9e0e6" ? 
					<button 
						type="submit" 
						className="btn btn-success" 
						onClick={() => {onPayHandler(booking._id)}}>Pay
					</button> : null
				}
				</td>
			</tr>
		</Fragment>
	))

	const noBookings = (
		<Fragment>
			<tr>
				<td colspan="8">No events attended.</td>
			</tr>
		</Fragment>
	)

	return (
		<Fragment>
		<div className="container mx-auto my-5 col-md-12" style={{height: "100vh"}}>
			<div className="row">
				<div className="col-md-12">
					<h1>Booking History</h1>
				</div>
			</div>

			<div className="row">
				<div className="col-md-11 mx-auto mt-4">
					<table className="table table-striped table-hover border-primary">
						<thead className="table-primary">
							<tr>
								<th>Reference No.</th>
								<th>Event</th>
								<th>Organizer</th>
								<th>Date Booked</th>
								<th>Attendees</th>
								<th>Total</th>
								<th>Status</th>
								<th>Payment</th>
								<th>Deletion</th>
							</tr>
						</thead>

						<tbody>
							{
								bookings.length !== 0 ? showBookings : noBookings
							}
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<Footer />
		</Fragment>
	)
}

export default ViewBookings;