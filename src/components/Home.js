import React, { useState, useEffect, Fragment } from "react";
import { Link } from "react-router-dom";
// import Swal from "sweetalert2";
import { Carousel, Card } from "react-bootstrap";
import { URL } from "../config";
import Footer from "./layouts/Footer";

const Home = ({events}) => {
  	// const [events, setEvents] = useState([])
	const [categories, setCategories] = useState([])
	const [users, setUsers] = useState([])
	const user = JSON.parse(localStorage.getItem("user"))

	// useEffect( () => {
	//   fetch(`${URL}/events`)
	//   .then(res => res.json())
	//   .then(data => {
	//     setEvents(data)
	//   })
	// }, [events]);

	useEffect( () => {
		fetch(`${URL}/categories`)
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})
	}, [categories])

	useEffect( () => {
		fetch(`${URL}/users`)
		.then(res => res.json())
		.then(data => {
			setUsers(data)
		})
	}, [users])

	const styleCarouselBg = {
		height: "100vh",
		width: "100vw"
	};

	const styleCarouselCaption = {
		backgroundColor: "rgba(255, 255, 255, .6)",
		fontFamily: "Indie Flower, cursive",
		letterSpacing: "2px",
		color: "black",
		width: "600px",
		borderRadius: "15px",
		textShadow: "2px 2px rgba(176, 176, 176, .7)"
	}

	const showEvents = events.map(event => (
		<Card className="col-10 mx-auto mt-4 mb-2 card-block" key={event._id} style={{border: "1px solid #3F51B5"}}>
			<div className="row mx-auto">
				<div className="col-7">
					<Carousel indicators="false">
						{event.images.map(img => (
							<Carousel.Item key={img}>
								<img
									src={`${URL}/images/${img}`}
									height="350px"
									width="100%"
									alt="..."
									style={{objectFit: "contain"}}
								/>
							</Carousel.Item>
						))}
					</Carousel>
				</div>

				<div className="col-5" style={{position: "relative"}}>
			  		<Card.Body>
					    <Card.Title>{event.name}</Card.Title>
					    	{users.map(user => (
					    		user._id === event.userId ?
					    		<Card.Text key={user._id}>{user.name}</Card.Text> : null
				    		))}
					    	{categories.map(category => (
					    		category._id === event.categoryId ? 
					    		<Card.Text key={category._id}>{category.name}</Card.Text> : null
				    		))}
				    		<p>{event.description}</p>
		  			</Card.Body>
		  			<Card.Footer style={{position: "absolute", bottom: "0", width: "90%"}}>
		  				{user ? 
		  					user.authId === "5e9bf8d4ffd86ed245693d53" ? 
						    	<Link to={`/events/${event._id}`} className="btn text-white w-100" style={{backgroundColor: "#3F51B5"}}>View Event</Link>
						    	: <Link to={`/events/${event._id}/booking`} className="btn text-white w-100" style={{backgroundColor: "#3F51B5"}}>View Event</Link>
						    : <Link to={`/events/${event._id}`} className="btn text-white w-100" style={{backgroundColor: "#3F51B5"}}>View Event</Link>
		  				}
		  			</Card.Footer>
	  			</div>
			</div>
		</Card>
	));

	return (
		<Fragment>

			<div className="container-fluid p-0 mb-5">
				<div className="row p-0 m-0">
					<Carousel className="col-12 m-0 p-0">
						<Carousel.Item>
							<img
								className="d-block img-fluid"
								src="https://blogmedia.evbstatic.com/wp-content/uploads/wpmulti/sites/8/2019/07/action-adults-celebration-433452.jpg"
								alt="First slide"
								style={styleCarouselBg}
							/>
							<Carousel.Caption className="mx-auto" style={styleCarouselCaption}>
								<h3><b>Tomorrowland 2019</b></h3>
								<p style={{fontFamily: "Raleway, sans-serif"}}>One of the largest music festivals out there.</p>
							</Carousel.Caption>
						</Carousel.Item>

						<Carousel.Item>
							<img
								className="d-block img-fluid"
								src="https://community.arm.com/cfs-filesystemfile/__key/communityserver-components-secureimagefileviewer/communityserver-blogs-components-weblogfiles-00-00-00-21-42/850_5F00_5671_5F00_optimised.jpg_2D00_900x506x2.jpg?_=636937032707184555"
								alt="Second slide"
								style={styleCarouselBg}
							/>

							<Carousel.Caption className="mx-auto p-1" style={styleCarouselCaption}>
								<h3><b>TechCon 2019</b></h3>
								<p style={{fontFamily: "Raleway, sans-serif"}}>TechCon® SE Asia is renowned for its international content, bringing experts from across the globe to share their knowledge and experiences.</p>
							</Carousel.Caption>
						</Carousel.Item>

						<Carousel.Item>
							<img
								className="d-block img-fluid"
								src="https://d2lfsu1qnyxzxu.cloudfront.net/cms/iStock-960571902-0_d2.jpg"
								alt="Third slide"
								style={styleCarouselBg}
							/>

							<Carousel.Caption className="mx-auto" style={styleCarouselCaption}>
								<h3><b>Tara Run nAPO</b></h3>
								<p style={{fontFamily: "Raleway, sans-serif"}}>A charity run powered by APO Runners to increase awareness and help raise funds for Learners with Special Education Needs (LSEN).</p>
							</Carousel.Caption>
						</Carousel.Item>
					</Carousel>
				</div>

				<div className="row" style={{minheight: "100vh"}}>
					<div className="col-10 mx-auto">
						<h1 className="mt-4" style={{fontFamily: "Indie Flower, cursive", color: "#3F51B5"}}><b>Events</b></h1>
						{
							events.length !== 0 ? showEvents : <h1 className="col-9 mx-auto alert alert-danger mt-5">No events posted as of now.</h1>
						}
					</div>
				</div>
			</div>

			<Footer />

		</Fragment>
	)
};

export default Home;