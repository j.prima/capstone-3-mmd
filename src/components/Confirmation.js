import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { URL } from "../config";
import { useParams } from "react-router-dom";

const Confirmation = ({user}) => {
	let {id} = useParams();
	let {token} = useParams();

	useEffect(() => {
		fetch(`${URL}/users/${id}/${token}`, {
			method: "PUT"
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	})

	return (
		<div className="alert alert-success col-9 mx-auto mt-5" role="alert">
			<h3>
				Thank you! You have successfully confirmed your email account. Please proceed to <Link to="/login">login</Link>.
			</h3>
		</div>
	)
};

export default Confirmation;