import React from "react";
import {Link} from 'react-router-dom';

const Auth = () => {
    return(
    	<div className="alert alert-success col-9 mx-auto mt-5" role="alert">
	    	<h3>Successfully Registered!</h3>
	        <h6>
	            Please check your email for the verfication link 
	            or <Link to="/login">Login</Link> here.
	        </h6>
    	</div>
    )
}

export default Auth;